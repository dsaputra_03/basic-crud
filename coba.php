<?php
session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'function.php';
$vrsnm = query("SELECT * FROM film");
// var_dump($vrsnm);
// die;

if (isset($_POST["cari"])) {
    $vrsnm = cari($_POST["keyword"]);
}

?>
<html>

<head>
</head>

<body>

    <form action="" method="post">
        <input type="text" autofocus name="keyword" placeholder="masukan keyword" autocomplete="off">
        <button type="submit" name="cari">cari</button>
    </form>
    <table style="width:100%">
        <tr>
            <th>#</th>
            <th>Id</th>
            <th>Judul</th>
            <th>Harga</th>
            <th>Durasi</th>
            <th>Expired</th>
            <th>Deksripsi</th>
        </tr>
        <?php $i = 1; ?>
        <?php foreach ($vrsnm as $row) : ?>
            <tr>
                <td>
                    <a href="edit.php?id=<?= $row["0"]; ?>">Edit</a> |
                    <a href="hapus.php?id=<?= $row["0"]; ?>" onclick="return confirm('yakin?')">Hapus</a>

                </td>
                <td>
                    <?= $row["id"] ?>
                </td>
                <td>
                    <?= $row["judul"] ?>
                </td>
                <td>
                    <?= $row["harga"] ?>
                </td>
                <td>
                    <?= $row["durasi"] ?>
                </td>
                <td>
                    <?= $row["expired"] ?>
                </td>
                <td>
                    <img src="img/<?php echo $row["deskripsi"]; ?>" width="50">
                </td>
            </tr>
            <?php $i++; ?>
        <?php endforeach ?>
    </table>
    <a href="tambah.php">Tambah Data</a><br>
    <a href="logout.php">logout</a>
</body>

</html>