<?php
session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}


require 'function.php';
if (isset($_POST["submit"])) {
    if (tambah($_POST) > 0) {
        echo "
            <script>
                alert('Data Berhasil Ditambah');
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data Gagal Ditambah');
            </script>
        ";
    }
}

?>
<html>

<head>
    <title>EDIT</title>
</head>

<body>
    <form action="" method="POST" enctype="multipart/form-data">
        <ul>
            <li>
                <label for="jdl">Judul</label>
                <input type="text" name="judul" id="jdl" required>
            </li>
            <li>
                <label for="hrg">Harga</label>
                <input type="text" name="harga" id="hrg" required>
            </li>
            <li>
                <label for="drsi">Durasi</label>
                <input type="text" name="durasi" id="drsi" required>
            </li>
            <li>
                <label for="ex">Expired</label>
                <input type="text" name="expired" id="ex" required>
            </li>
            <li>
                <label for="deks">Deskripsi</label>
                <input type="file" name="deksripsi" id="deks">
            </li>
        </ul>
        <button type="submit" name="submit">Tambah</button>
        <a href="coba.php">lihat list</a>
    </form>

</body>

</html>