<?php
session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}


require 'function.php';
$id = $_GET["id"];

$film = query("SELECT * FROM film WHERE id = $id")[0];


if (isset($_POST["submit"])) {
    if (ubah($_POST) > 0) {
        echo "
            <script>
                alert('Data Berhasil Diubah');
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data Gagal Diubah');
            </script>
        ";
    }
}

?>
<html>

<head>
    <title>EDIT</title>
</head>

<body>
    <form action="" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $film["id"]; ?>">
        <input type="hidden" name="gambarlama" value="<?= $film["deskripsi"]; ?>">
        <ul>
            <li>
                <label for="jdl">Judul</label>
                <input type="text" name="judul" id="jdl" required value="<?= $film["judul"]; ?>">
            </li>
            <li>
                <label for="hrg">Harga</label>
                <input type="text" name="harga" id="hrg" required value="<?= $film["harga"]; ?>">
            </li>
            <li>
                <label for="drsi">Durasi</label>
                <input type="text" name="durasi" id="drsi" required value="<?= $film["durasi"]; ?>">
            </li>
            <li>
                <label for="ex">Expired</label>
                <input type="text" name="expired" id="ex" required value="<?= $film["expired"]; ?>">
            </li>
            <li>
                <label for="deks">Deskripsi</label><br>
                <img src="img/<?= $film["deskripsi"]; ?>" width="50"><br>
                <input type="file" name="deksripsi" id="deks">
            </li>
        </ul>
        <button type="submit" name="submit">Ubah</button>
        <a href="coba.php">lihat list</a>
    </form>
</body>

</html>