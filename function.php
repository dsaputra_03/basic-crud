<?php
$conn = mysqli_connect("localhost", "root", "", "versinema");


function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_array($result)) {
        $rows[] = $row;
    }
    return $rows;
}

//htmlspecialchars() untuk mencegah pengisian script kedalam textfield 
function tambah($data)
{
    global $conn;
    $jdl = htmlspecialchars($data["judul"]);
    $hrga = htmlspecialchars($data["harga"]);
    $drs = htmlspecialchars($data["durasi"]);
    $xprd = htmlspecialchars($data["expired"]);

    // $dks = htmlspecialchars($data["deksripsi"]);
    //upload gambar 
    $dks = upload();
    if (!$dks) {
        return false;
    }

    $query = "INSERT INTO  film
                VALUES
                ('', '$jdl', '$hrga', '$drs', '$xprd', '$dks')";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}


function upload()
{
    $namafile = $_FILES['deksripsi']['name'];
    $ukuranfile = $_FILES['deksripsi']['size'];
    $error = $_FILES['deksripsi']['error'];
    $tmpname = $_FILES['deksripsi']['tmp_name'];

    // cek apakah tidak ada yg diupload
    if ($error === 4) {
        echo "<script>
                alert('pilih gambar dulu');
            </script>";
        return false;
    }

    $ekstensigambarvalid = ["jpg", "jpeg", "png"];
    $ekstensigambar = explode(".", $namafile);
    $ekstensigambar = strtolower(end($ekstensigambar));

    if (!in_array($ekstensigambar, $ekstensigambarvalid)) {
        echo "<script>
            alert('yg anda upload bukan gambar');
        </script>";
        return false;
    }

    if ($ukuranfile > 10000000) {
        echo "<script>
            alert('ukuran terlalu besar');
        </script>";
        return false;
    }

    move_uploaded_file($tmpname, 'img/' . $namafile);

    return $namafile;
}

function hapus($id)
{
    global $conn;

    mysqli_query($conn, "DELETE FROM film WHERE id = $id");

    return mysqli_affected_rows($conn);
}

function ubah($data)
{
    global $conn;

    $id = $data["id"];
    $jdl = htmlspecialchars($data["judul"]);
    $hrga = htmlspecialchars($data["harga"]);
    $drs = htmlspecialchars($data["durasi"]);
    $xprd = htmlspecialchars($data["expired"]);
    $dkslama = $data["gambarlama"];

    if ($_FILES['deksripsi']['error'] === 4) {
        $dks = $dkslama;
    } else {
        $dks = upload();
    }



    $query = "UPDATE film SET 
                    judul = '$jdl',
                    harga = '$hrga',
                    durasi = '$drs',
                    expired = '$xprd',
                    deskripsi = '$dks'
                    WHERE id = $id
                    ";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function cari($keyword)
{
    $query = "SELECT * FROM film WHERE judul LIKE '%$keyword%' OR durasi LIKE '%$keyword'";

    return query($query);
}

function registrasi($data)
{
    global $conn;
    //stripslashes berfungsi menghilangkan slash
    //strtolower berfungsi mengubah tulisan menjadi kecil
    //mysqli_real_escape_string berfungsi agar karakter seperti tanda kutip dapat tersimpan di database / dibaca string
    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);

    $result = mysqli_query($conn, "SELECT username FROM users WHERE username = '$username'");
    if (mysqli_fetch_assoc($result)) {
        echo "<script>
            alert('username sudah ada');
            </script>";

        return false;
    }
    if ($password !== $password2) {
        echo "<script>
            alert('password beda');
            </script>";

        return false;
    }

    //enkripsi
    $password = password_hash($password, PASSWORD_DEFAULT);

    mysqli_query($conn, "INSERT INTO users VALUES('', '$username', '$password')");
    return mysqli_affected_rows($conn);
}
